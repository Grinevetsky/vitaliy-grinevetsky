import XCTest

import CurrencyNetworkTests

var tests = [XCTestCaseEntry]()
tests += CurrencyNetworkTests.allTests()
XCTMain(tests)
