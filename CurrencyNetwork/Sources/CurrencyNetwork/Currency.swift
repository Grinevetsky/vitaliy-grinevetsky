//
//  Currency.swift
//  WestpacTest
//
//  Created by Vitaliy Grinevetsky on 31/8/20.
//  Copyright © 2020 Vitaliy Grinevetsky. All rights reserved.
//

import Foundation

public struct Currency: Decodable {
    
    public var currencyCode: String
    public var currencyName: String
    public var country: String
    public var buyTT: Double?
    public var sellTT: Double?
    public var buyTC: Double?
    public var buyNotes: Double?
    public var sellNotes: Double?
    public var spotRateDate: Date?
    public var effectiveDate: Date?
    public var updateDate: Date?
    public var lastUpdated: Date?
    
    enum CodingKeys: String, CodingKey, CaseIterable {
        
        case currencyCode
        case currencyName
        case country
        case buyTT
        case sellTT
        case buyTC
        case buyNotes
        case sellNotes
        case spotRateDate = "SpotRate_Date_Fmt"
        case effectiveDate = "effectiveDate_Fmt"
        case updateDate = "updateDate_Fmt"
        case lastUpdated = "LASTUPDATED"
    }
    
    // decoder override to handle dates and numbers
    public init(from decoder: Decoder) throws {

        let container = try decoder.container(keyedBy: CodingKeys.self)
        
        self.currencyCode = try container.decode(String.self, forKey: .currencyCode)
        self.currencyName = try container.decode(String.self, forKey: .currencyName)
        self.country = try container.decode(String.self, forKey: .country)
        self.buyTT = container.doubleIfPresent(forKey: .buyTT)
        self.sellTT = container.doubleIfPresent(forKey: .sellTT)
        self.buyTC = container.doubleIfPresent(forKey: .buyTC)
        self.buyNotes = container.doubleIfPresent(forKey: .buyNotes)
        self.sellNotes = container.doubleIfPresent(forKey: .sellNotes)
        self.spotRateDate = container.dateIfPresent(forKey: .spotRateDate)
        self.effectiveDate = container.dateIfPresent(forKey: .effectiveDate)
        self.updateDate = container.dateIfPresent(forKey: .updateDate)
        self.lastUpdated = container.dateIfPresent(forKey: .lastUpdated)
    }
}
public struct CurrencyResponse: Decodable {
    
    public private(set) var currencies: [Currency] = [Currency]()
    
    public init(from decoder: Decoder) throws {
        
        currencies.removeAll()
        
        let container = try decoder.container(keyedBy: RootKeys.self)
        
        if let dataContainer = try? container.nestedContainer(keyedBy: DataKeys.self, forKey: .data),
            let brandsContainer = try? dataContainer.nestedContainer(keyedBy: BrandsKeys.self, forKey: .brands),
            let wbcContainer = try? brandsContainer.nestedContainer(keyedBy: WBCKeys.self, forKey: .wbc),
            let portfoliosContainer = try? wbcContainer.nestedContainer(keyedBy: PortfoliosKeys.self, forKey: .portfolios),
            let fxContainer = try? portfoliosContainer.nestedContainer(keyedBy: FXKeys.self, forKey: .fx),
            let productsContainer = try? fxContainer.nestedContainer(keyedBy: CustomCodingKeys.self, forKey: .products) {
            
            for key in productsContainer.allKeys {
                
                if let productContainer = try? productsContainer.nestedContainer(keyedBy: ProductKeys.self, forKey: key),
                    let rateContainer = try? productContainer.nestedContainer(keyedBy: CustomCodingKeys.self, forKey: .rates){
                    
                    rateContainer.allKeys.forEach{
                        guard let value = try? rateContainer.decode(Currency.self, forKey: $0) else {
                            return
                        }
                        self.currencies.append(value)
                    }
                }
            }
        }
    }
}


private enum RootKeys: String, CodingKey {
    case status
    case data
}

private enum DataKeys: String, CodingKey {
    case brands = "Brands"
}

private enum BrandsKeys: String, CodingKey {
    case wbc = "WBC"
}

private enum WBCKeys: String, CodingKey {
    
    case brand = "Brand"
    case portfolios = "Portfolios"
}

private enum PortfoliosKeys: String, CodingKey {
    
    case fx = "FX"
}

private enum FXKeys: String, CodingKey {
    
    case portfolioId = "PortfolioId"
    case products = "Products"
}

private enum ProductKeys: String, CodingKey {
    
    case productId = "ProductId"
    case rates = "Rates"
}

/// Handles custom dynamic keys
private struct CustomCodingKeys: CodingKey {
    
    var stringValue: String
    var intValue: Int?
    
    init?(stringValue: String) {
        
        self.stringValue = stringValue
    }
    
    init?(intValue: Int) {
        
        return nil
    }
}
