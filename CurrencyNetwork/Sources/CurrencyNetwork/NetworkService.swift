//
//  NetworkService.swift
//  WestpacTest
//
//  Created by Vitaliy Grinevetsky on 28/8/20.
//  Copyright © 2020 Vitaliy Grinevetsky. All rights reserved.
//

import Foundation

/// Network Service class, which is responsible for creating URLSession and and sending requests
public class NetworkService: NSObject, URLSessionDelegate {
    
    private(set) var session: URLSession!
    private let baseURL: URL?

    /// Init method
    /// - Parameter baseURL,  url host
    /// - Parameter sessionConfiguration,  URLSessionConfiguration,
    public init(baseURL: URL? = nil, sessionConfiguration:URLSessionConfiguration = .default) {
        self.baseURL = baseURL
        super.init()
        session = URLSession(configuration: sessionConfiguration, delegate: self, delegateQueue: nil)
        session.sessionDescription = UUID().uuidString
    }
    
    /// Method for sending request
    /// - Parameter request, is request object
    /// - Parameter completion, is completion block after proceeding request, returns Result<ResponseObject,Error>
    public func execute<RequestType>(request: RequestType, completion: @escaping(Result <RequestType.ResponseType, HttpError>) -> Void) where RequestType: TypeResponseNetworkRequest, RequestType: NetworkRequest{
        
        self.execute(request: request, completion: { (request, error) in
            if let error = error {
                completion(.failure(error))
            }else if let response = request.responseObject {
                completion(.success(response))
            }else {
                let error = HttpError.noData(nil)
                completion(.failure(error))
            }
        })
    }
    
    ///  Method for actual sending request via URLSessionDataTask
    /// - Parameter request, is request object
    /// - Parameter completion, is completion block after proceeding request, returns tuple (ResponseObject,Error)
    public func execute<RequestType>(request: RequestType, completion: @escaping  (RequestType, HttpError?) -> ()) where RequestType: NetworkRequest {
        
        if let baseUrl = self.baseURL {
            request.setBaseURL(url: baseUrl)
        }
        do {
            try request.prepareForAPIUse()
        }catch{
            completion(request, HttpError.encodeRequestBody(error))
        }
        if request.urlRequest.url?.scheme == nil {
            completion(request, HttpError.malformedRequest)
        }
        let task: URLSessionDataTask = self.session.dataTask(with: request.url){ (data, urlResponse, error) in
            self.didFinishRequest(request: request, data: data, urlResponse: urlResponse, error: error, completion:completion)
        }
        task.resume()
    }
    
    /// Method for proceeding with response after executing a request
    /// - Parameter request, request object
    /// - Parameter data, pure Data as server response
    /// - Parameter urlResponse, URLResponse
    /// - Parameter error, error object
    /// - Parameter completion,  is completion block after proceeding request
    private func didFinishRequest<RequestType>(request: RequestType, data: Data?, urlResponse: URLResponse?, error: Error?, completion: @escaping (RequestType, HttpError?) -> ()) where RequestType: NetworkRequest{
        request.httpResponse = urlResponse as? HTTPURLResponse
        
        var resolvedError: HttpError? = nil
        if urlResponse == nil {
            resolvedError = HttpError.noData(nil)
        }
        
        do {
            try request.setResponseData(data: data)
        }catch{
            resolvedError = HttpError.noData(urlResponse)
        }
        
        DispatchQueue.main.async{
            completion(request, resolvedError)
        }
    }
    
    
    public func urlSession(_ session: URLSession, didReceive challenge: URLAuthenticationChallenge, completionHandler complitionHandler: @escaping (URLSession.AuthChallengeDisposition, URLCredential?) -> Void) {
        
        complitionHandler(.performDefaultHandling, nil)
    }
    
    /// Default de-init method
    /// Will invalidate and cancel operations in session in case of deallocation
    deinit{
        session.invalidateAndCancel()
    }
}

