//
//  DateFormatter.swift
//  WestpacTest
//
//  Created by Vitaliy Grinevetsky on 31/8/20.
//  Copyright © 2020 Vitaliy Grinevetsky. All rights reserved.
//

import Foundation


extension DateFormatter {
    
    /// returns a formatter for `yyyyMMdd`
    public static let dateFormatter: DateFormatter = {
        let dateFormatter = DateFormatter()
        dateFormatter.locale = .current
        dateFormatter.dateFormat = "yyyyMMdd"
        return dateFormatter
    }()
    
    ///  returns a formatter for   `yyyyMMdd'T'HHmmss,SSSZZZZZ`
    public static let extendedDateFormatter: DateFormatter = {
        let dateFormatter = DateFormatter()
        dateFormatter.locale = .current
        dateFormatter.dateFormat = "yyyyMMdd'T'HHmmss,SSSZZZZZ"
        return dateFormatter
    }()
    
    ///  returns a formatter for  `HH:mm a dd MMM yyyy`
    public static let timeAndDateFormatter: DateFormatter = {
        let dateFormatter = DateFormatter()
        dateFormatter.locale = .current
        dateFormatter.dateFormat = "HH:mm a dd MMM yyyy"
        
        return dateFormatter
    }()
    
    ///  returns a formatter for `dd/MM/yyyy HH:mm a`
    public static let dateAndTimeFormatter: DateFormatter = {
        let dateFormatter = DateFormatter()
        dateFormatter.locale = .current
        dateFormatter.dateFormat = "MMM dd, yyyy hh:mm a"
        return dateFormatter
    }()
}
