//
//  KeyedContainer.swift
//  WestpacTest
//
//  Created by Vitaliy Grinevetsky on 31/8/20.
//  Copyright © 2020 Vitaliy Grinevetsky. All rights reserved.
//

import Foundation

extension KeyedDecodingContainer {
    
    /// Decode numbers based on the formats thrown by the API
    /// returns `nil` if the value is not a number e.g. "N/A"
    /// - Parameter key: the `CodingKey` attribute name
    func doubleIfPresent(forKey key: KeyedDecodingContainer.Key) -> Double? {
        
        guard let string = try? self.decodeIfPresent(String.self, forKey: key) else {
            
            return nil
        }
        return Double(string)
    }
    
    /// Decode numbers based on the formats thrown by the API
    /// returns `nil` if the value has unknown format
    /// - Parameter key: the `CodingKey` attribute name
    func dateIfPresent(forKey key: KeyedDecodingContainer.Key) -> Date? {
        guard let string = try? self.decodeIfPresent(String.self, forKey: key) else {
            return nil
        }
        return string.toDate()
    }
    
}
