//
//  NetworkRequest.swift
//  WestpacTest
//
//  Created by Vitaliy Grinevetsky on 28/8/20.
//  Copyright © 2020 Vitaliy Grinevetsky. All rights reserved.
//

import Foundation

/// Helper enum for HTTP Methods
public enum HttpRequestMethod: String, CaseIterable {
    case get = "GET"
    case put = "PUT"
    case post = "POST"
    case delete = "DELETE"
}

/// Network request class, whicjh is responsible for creating URLRequest,
/// http method, encoding body
open class NetworkRequest {
    
    public var urlRequest: URLRequest
    public var url: URL {
        return urlRequest.url!
    }
    
    
    public var method: HttpRequestMethod{
        get {
            return HttpRequestMethod(rawValue: self.urlRequest.httpMethod ?? "GET")!
        }
        set {
            self.urlRequest.httpMethod = newValue.rawValue
        }
    }
    
    public var httpResponse: HTTPURLResponse?
    
    public var httpStatusCode: Int {
        return httpResponse?.statusCode ?? 0
    }
    
    public init(urlString: String) {
        let url = URL(string: urlString.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? "")!
        self.urlRequest = URLRequest(url: url)
        self.urlRequest.cachePolicy = .reloadIgnoringLocalCacheData
    }
    
    public func setBaseURL(url: URL) {
        if urlRequest.url?.scheme != nil {
            return // Schema  is already set don't change it
        }
        let fullURL = "\(url.absoluteString)/\(self.url.absoluteString)"
        urlRequest.url = URL(string: fullURL)!
    }
    
    /// Method for encoding body parameters
    /// throw exeption in case of failure
    func prepareForAPIUse() throws { }
    
    /// Method for attaching response
    /// throw exeption in case of failure
    func setResponseData(data: Data?) throws { }
    
    
    /// Method for add additional header parameters ( standard key-value HTTP header)
    /// - Parameter header: String
    /// - Parameter value: String
    public func add(header: String, value: String) {
        self.urlRequest.addValue(value, forHTTPHeaderField: header)
    }
}

///Type used for requests with no presponse
public struct EmptyResponseModel: Decodable {
    public init() {
    }
}

///Type used for requests with no request'
public struct EmptyRequestModel: Encodable{
    public init() {
    }
}

///Generic protocol for any request which has a fixed type
public protocol TypeResponseNetworkRequest {
    associatedtype ResponseType
    
    var responseObject: ResponseType? { get }
}

/// Generic method for JSON network Request
/// - Parameter RequestType - generic request object, must be Encodable
/// - Parameter ResponseType - generic response object, must be Decodable
public class JSONNetworkRequest<RequestType, ResponseType> : NetworkRequest, TypeResponseNetworkRequest where RequestType: Encodable, ResponseType: Decodable {
    
    public typealias ResponseType = ResponseType
    
    ///The request object to be sent with Request
    public var requestBodyObject: RequestType?
    
    //The response object parsed after a successful request
    public var responseObject: ResponseType?
    
    
    public init(path: String, body: RequestType? = nil) {
        self.requestBodyObject = body
        super.init(urlString: path)
        self.urlRequest.setValue("application/json", forHTTPHeaderField: "Content-Type")
        self.urlRequest.setValue("application/json", forHTTPHeaderField: "Accept")
    }
    
    
    override func prepareForAPIUse() throws{
        if let requestObj = requestBodyObject {
            let encoder  = JSONEncoder()
            self.urlRequest.httpBody = try encoder.encode(requestObj)
        }
    }
    
    override func setResponseData(data: Data?) throws {
        if (ResponseType.self == EmptyResponseModel.self) {
            self.responseObject = EmptyResponseModel() as? ResponseType
        }
        
        guard let data = data else {
            throw HttpError.noData(nil)
        }
        
        do {
            let decoder = JSONDecoder()
            self.responseObject = try decoder.decode(ResponseType.self, from: data)
        } catch  {
            throw HttpError.decoding(error)
        }
    }
}

///Conenience type for get requests that would never have a body
public class JSONNetworkGetRequest<ResponseType>: JSONNetworkRequest<EmptyRequestModel, ResponseType> where ResponseType: Decodable {
}
