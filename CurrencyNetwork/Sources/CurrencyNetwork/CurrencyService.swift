//
//  CurrencyService.swift
//  WestpacTest
//
//  Created by Vitaliy Grinevetsky on 28/8/20.
//  Copyright © 2020 Vitaliy Grinevetsky. All rights reserved.
//

import Foundation

private struct CurrencyRequest{
    
    static func getCurrency() -> JSONNetworkGetRequest<CurrencyResponse> {
        let path = "/bin/getJsonRates.wbc.fx.json"
        let request = JSONNetworkGetRequest<CurrencyResponse>(path: path)
        return request
    }
}

public typealias CurrencyCompletion = (Result<CurrencyResponse, HttpError>) -> ()

public final class CurrencyService {
    private let network: NetworkService
    
   public init(network: NetworkService){
        self.network = network
    }
    
    public func getCurrencies(completion: @escaping CurrencyCompletion ){
        let request = CurrencyRequest.getCurrency()
        network.execute(request: request, completion: completion)
    }
}
